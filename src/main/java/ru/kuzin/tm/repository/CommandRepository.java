package ru.kuzin.tm.repository;

import ru.kuzin.tm.api.ICommandRepository;
import ru.kuzin.tm.constant.ArgumentConst;
import ru.kuzin.tm.constant.CommandConst;
import ru.kuzin.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command HELP = new Command(CommandConst.HELP, ArgumentConst.HELP, "Show command list.");

    private static final Command VERSION = new Command(CommandConst.VERSION, ArgumentConst.VERSION, "Show version info.");

    private static final Command ABOUT = new Command(CommandConst.ABOUT, ArgumentConst.ABOUT, "Show developer info.");

    private static final Command INFO = new Command(CommandConst.INFO, ArgumentConst.INFO, "Show system info.");

    private static final Command EXIT = new Command(CommandConst.EXIT, null, "Close application.");

    private static final Command[] COMMANDS = new Command[] {
            HELP, VERSION, ABOUT, INFO, EXIT
    };

    public Command[] getCommands() {
        return  COMMANDS;
    }

}
